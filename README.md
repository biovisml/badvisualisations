# BadVisualisations

## Introduction

This Shiny app was developed to replicate common data visualisation errors. It contains six examples: _Highlighting and Scale, Pie Charts, Proportions, Clutter, Heatmaps_ and _3D Visualisations_. Each example is visualised in its own tab. The app was originally created to support a [Bioinformatics Winter School talk](http://bioinformatics.org.au/winterschool/speaker-items/bio-data-analysis-and-visualisation-hogan/) given by Jim Hogan at the University of Queensland in 2018. You will find the original [Winter School presentation here](https://docs.google.com/presentation/d/1fmF0yUsdN5Q7DE480ZLm5PeR39euyIo_Y_cEbdbJgro/edit?usp=sharing) and we will soon submit a pdf to BioArxiv exploring the application and the general ideas in more detail.  

If you want to explore the Shiny app you will need to install a recent version of R&mdash;the app was created under version 3.4.4 and most recently tests under v.4.0.2. The required packages are: `shiny,shinythemes,readr,ggplot2,plyr,dplyr,DT,tools,readxl,reshape2,tidyr,scales,cowplot,shinydashboard` and `shinywidgets`. Once these packages are installed, you will be able to just open  app.R and run the app from RStudio. 

A hosted version of the Bad Visualisation app will shortly be available from the [QUT Centre for Data Science](https://research.qut.edu.au/qutcds/) Shiny App server. The URL will be linked here when it is live, but will likely be: https://qutcds.shinyapps.io/badViz_app/. 

Suggestions for additional topics or offers to contribute are more than welcome. Please raise an issue here or [email us](mailto:j.hogan@qut.edu.au).

## Visualisations

### Highlighting and Scale

### ![Highlighting and Scale](images/Highlighting.png)

The 'Highlighting and Scale" tab starts out as a bad visualisation illustrating problems with y-axis scaling and highlighting. As the slider progresses the visualisation gets better by stretching out the y-axis and changing the colouring.

Note: The data used in this visualisation is dummy data.

### Pie Charts

![Pie Charts](images/Pie_Charts.png)

The 'Pie Charts' tab initally displays two pie charts for 'Agressive' and 'Non-Aggressive' cancer cases. Moving the slider to the right improves the quality of the visualisation, but the example shows the limitations of a pie chart representation. 

Note: The data used for this visualisation is based on cancer research data.

### Proportions

![Proportions](images/Proportions.png)

The proportions tab relies on publicly available metagenomics data. Initially, the display shows the full relative abundance of Genera as a bar chart with the 'Correct Normalisation' box checked.

If a Genus is deselected it is removed from the graph. There is also the option to include the total relative frequency of the remaining Genera as one bar labeled 'Not Selected'. If you remove a Genus and 'Correct Normalisation' is unchecked, the data for the selected Genus is recalculated based on the relative frequency of the selected Genus e.g. the frequency is divided by the 'Selected Genera' total rather than the whole total.

Note: The data used for this visualisation is based on public metagenomic data from the EBI Repository, clustered and filtered according to 16S match quality. The ten most abundant genera are named. The label Not Considered aggregates the figures for all of the remaining clusters, including those for which no reliable match was found.

### Clutter

 ![Clutter](images/Clutter.png)

This Clutter tab contains two visualisations, a scatterplot and a box-and-whisker plot, each displayed in a separate tab. Each visualisation contains filters that can be used to declutter each visualisation. Filters include selecting airlines and causes of delays to be displayed, as well as a slider to rescale the axes. The box-and-whisker plot also allows you to remove outliers by selecting the checkbox.

Note: This visualisation uses American Airline Delay data from the Bureau of Transportation Statistics, Airline Service Quality Performance 234 in Janurary 2018.

### Heatmaps

![Heatmaps](images/Heatmaps.png)

The Heatmaps slider combines samples from two different sources, with a substantial difference&mdash;initially 10&mdash;between their means. As the slider moves to the right, the difference decreases until the sources have the same mean, with the heatmap colouring reflecting these changes. The midpoint of the colouring is chosen to lie halfway between the means of the two samples, and this break may not correspond to any significant difference in the data values. Once the slider reaches the end you are given the option to color the heat map _correctly_ by checking the Correct Colour box.

Note: The data used for this visualisation is randomly generated using R sampling functions. 

### 3D Visualisations

![3DViz](images/3DViz.png)

The slider on the current visualisation transforms the data from low density to high density, showing some of the limitations of the 3D view. 

Note: The data used in this visualisation was randonly generated
