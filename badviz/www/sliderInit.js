$(document).ready(function() {
    $("#Improve").ionRangeSlider({
            values: ["Most misleading", "1", "2", "Least misleading"],
            hide_min_max: false,
        	hide_from_to: true
    });
        $("#Improvements").ionRangeSlider({
            values: ["Pie charts", "Stacked bar", "Bar charts", "Solution one", "Solution two"],
            hide_min_max: false,
        	hide_from_to: false
    });
         $("#Colour").ionRangeSlider({
            values: ["Distinct", "1", "2","3", "4", "5", "6", "7", "8", "9", "Mixture"],
            hide_min_max: false,
            hide_from_to: true
    });
        $("#skew").ionRangeSlider({
            values: ["400", "350", "300", "250", "200", "150", "100", "50"],
            hide_min_max: false,
            hide_from_to: false
    });
    });