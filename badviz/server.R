# This is the server logic of a Shiny web application. You can run the 
# application by clicking 'Run App' above.

library(shiny)
library(shinydashboard)
library(shinythemes)
library(ggplot2)
library(dplyr)
library(DT)
library(tools)
library(RColorBrewer)
library(readxl)
library(reshape2)
library(tidyr)
library (scales)
library(latticeExtra)
library(cowplot)
theme_set(theme_minimal())
source("3D_helper.R")

# Define server logic required to draw a histogram
shinyServer(function(input, output) {
  
  #--------    Fraud and Highlighting ------------ #
  
  #Graph One
  PractiseData <-
    data.frame(
      Model = c("Their Model 1", "Their Model 2", "Our Model 1", "Our Model 2"),
      Accuracy = c(0.589415, 0.590705, 0.590867, 0.592849)
    )
  
  output$barGraph <- renderPlot({
    if (input$Improve == 0) {
      ggplot(data = PractiseData, aes(x = Model, y = Accuracy, fill = Model)) + geom_bar(stat ="identity") +
        coord_cartesian(ylim = c(0.585, 0.595)) + scale_fill_manual(values = c("#FF8888", "#FF0000", "Gray", "Gray"))
    }
    else if (input$Improve == "1") {
      ggplot(data = PractiseData, aes(x = Model, y = Accuracy, fill = Model)) + 
        geom_bar(stat = "identity") +
        coord_cartesian(ylim = c(0, 0.595)) + scale_fill_manual(values = c("#FF8888", "#FF0000", "Gray", "Gray"))
    } else if (input$Improve == "2") {
      ggplot(data = PractiseData, aes(x = Model, y = Accuracy, fill = Model)) + geom_bar(stat =
                                                                                           "identity") +
        coord_cartesian(ylim = c(0, 1)) + scale_fill_manual(values = c("#FF8888", "#FF0000", "Gray", "Gray"))
    } else {
      ggplot(data = PractiseData, aes(x = Model, y = Accuracy, fill = Model)) + geom_bar(stat =
                                                                                           "identity") +
        coord_cartesian(ylim = c(0, 1)) + scale_fill_manual(values = c("lightskyblue3", "lightskyblue3", "yellow4", "yellow4"))
    }  
  }, bg="transparent", execOnResize = TRUE)
  
  #----------- Pie Charts --------------# 
  
  #Tab plot 2 - Stacked Bar Graph 1
  DF <-
    read.table(text = "Tier ATM ATR BRCA2 CHEK2 MSH6 NBN PALB2  PMS2 POLD1 PRSS1 RAD51D SLX4 XRCC2 No_Mutation
               One 1 4 1 5 0 1 4 1 1 0 1 1 2 1 0", header = TRUE)
  DF1 <- melt(DF, id.var = "Tier")
  
  #Tab 2 - Pie Chart One
  StackedBarOne <- read_excel("CancerDataReplicated.xlsx")
  
  #AGGRESSIVE Data for Pie & Bar Chart together VIZ 
  chart <- read.table(text = "Name No_Mutation Others
                      'Aggressive Cases' 85 15", header = TRUE)
  chart <- melt(chart, id.var = "Name")
  
  chart2 <-
    read.table(text = "Name ATM ATR BRCA2 NBN PALB2  PMS2 POLD1 PRSS1 RAD51D SLX4 XRCC2
               'Aggressive Cases' 2 1 2 2 1 1 1 1 1 1 1",
               header = TRUE)
  newData <- melt(chart2, id.var = "Name")
  
  #NON-AGGRESSIVE Data for Pie & Bar Chart together VIZ 
  Pchart <- read.table(text = "Name No_Mutation Others
                       'Aggressive Cases' 90 10", header = TRUE)
  Pchart <- melt(Pchart, id.var = "Name")
  
  Pchart2 <-
    read.table(text = "Name ATM  BRCA2  MSH6 SLX4
               'Aggressive Cases' 2 2 4 2 ",
               header = TRUE)
  Pchart2 <- melt(Pchart2, id.var = "Name")
  
  #Two Pie Charts Together
  Pie_Tog <-
    read.table(
      text = "Name ATM ATR BRCA2 CHEK2 MSH6 NBN PALB2  PMS2 POLD1 PRSS1 RAD51D SLX4 XRCC2 No_Mutation
      'Aggressive Cases' 2 2 2 0 0 2 1 1 1 1 1 1 1 85
      'Non-Aggressive Cases'2 0 2 0 4 0 0 0 0 0 0 2 0 90 ",
      header = TRUE
    )
  Pie_Tog1 <- melt(Pie_Tog, id.var = "Name")
  
  ## BASE GRAPH PIE CHART ONE
  
  g1 <-
    ggplot(data = StackedBarOne, aes(x = "" , y = Proportion1, fill = Panel)) +
    geom_bar(width = 1, stat = "identity") + ggtitle("Aggressive Cases") +
    theme(
      plot.title = element_text(hjust = 0.5),
      legend.position = "none",
      axis.text = element_blank(),
      axis.ticks = element_blank(),
      axis.title.y = element_blank(),
      axis.title.x = element_blank(),
      panel.grid.major = element_blank(),
      panel.grid.minor = element_blank(),
      panel.background = element_blank(), 
      axis.line = element_blank()
    ) +
    scale_fill_manual(
      values = c(
        "Black",
        "Grey",
        "Red",
        "Blue",
        "Green",
        "Brown",
        "lemonchiffon1",
        "Purple",
        "Light Grey",
        "Light Green",
        "Violet",
        "Turquoise",
        "Orange",
        "Yellow"
      )
    )
  
  #BASE GRAPH PIE CHART 2
  
  g2 <-
    ggplot(data = StackedBarOne, aes(x = "" , y = Proportion2, fill = Panel)) +
    geom_bar(width = 1, stat = "identity") + ggtitle("Non-aggressive Cases") +
    theme(
      plot.title = element_text(hjust = 0.5),
      axis.text  = element_blank(),
      axis.ticks  = element_blank(),
      axis.title.y = element_blank(),
      axis.title.x = element_blank(),
      panel.grid.major = element_blank(),
      panel.grid.minor = element_blank(),
      panel.background = element_blank(),
      axis.line = element_blank()
    ) +
    scale_fill_manual(
      values = c(
        "ATM" = "Black",
        "ATR" = "Grey",
        "BRCA2" = "Red",
        "CHEK2" = "Blue",
        "MSH6" = "Green",
        "NBN" = "Brown",
        "NO MUTATION" = "lemonchiffon1",
        "PALB2" = "Purple",
        "PMS2" = "Light Grey",
        "POLD1" = "Light Green",
        "PRSS1" = "Violet",
        "RAD61D" = "Turquoise",
        "SLX4" = "Orange",
        "XRCC2" = "Yellow"
      )
    )
  
  # Code used to Create the new Viz (In the shiny app, we are using an image however this code was used to create that image)
  output$NewViz <- renderPlot({
    legend <- get_legend(ABC)
    plot_grid(
      plot_grid(plot_grid(plot_grid(NULL, A, rel_widths = c(.6, .4)), plot_grid(B, NULL, rel_widths = c(.6, .4)) , rel_heights = c(2, 7), ncol = 1),
                plot_grid(plot_grid(NULL, D, rel_widths = c(.6, .4)), plot_grid(C, NULL, rel_widths = c(.6, .4)) , rel_heights = c(2, 7), ncol = 1)),
      legend, rel_widths = c(9, 1))
  }, bg="transparent", execOnResize = TRUE)
  
  #Bar Graph
  output$BarGraph_Pie1 <- renderPlot({
    ggplot (data = Pie_Tog1, aes (x = Name, y = value, fill = variable)) +
      geom_bar(stat = "identity",
               width = 0.7,
               position = "dodge") +
      scale_fill_manual(
        values = c(
          "ATM" = "Black",
          "ATR" = "Grey",
          "BRCA2" = "Red",
          "CHEK2" = "Blue",
          "MSH6" = "Green",
          "NBN" = "Brown",
          "No_Mutation" = "lemonchiffon1",
          "PALB2" = "Purple",
          "PMS2" = "Light Grey",
          "POLD1" = "Light Green",
          "PRSS1" = "Violet",
          "RAD61D" = "Turquoise",
          "SLX4" = "Orange",
          "XRCC2" = "Yellow"
        )
      )
  }, bg="transparent", execOnResize = TRUE)
  
  #Pie Charts
  output$PieCharts <- renderPlot({
    legend <- get_legend(g2)
    plot_grid(plot_grid(g1 + coord_polar("y", start = 0), g2 + coord_polar("y", start = 0) + theme(legend.position = "none"), ncol =2), 
              legend, ncol = 2, rel_widths = c(0.9, 0.1))
  }, bg="transparent", execOnResize = TRUE)
  
  #Stacked Bar Graph with Pie Chart Data
  output$together <- renderPlot ({
    ggplot(data = Pie_Tog1 , aes(
      x = Name,
      y = value,
      fill = variable,
      hue = 0.2
    )) +
      geom_bar(width = 0.7, stat = "identity") +
      theme(
        legend.title = element_blank()
        # panel.grid.major = element_blank(),
        # panel.grid.minor = element_blank(),
        # panel.background = element_blank()
      )  +
      scale_fill_manual(
        values = c(
          "ATM" = "Black",
          "ATR" = "Grey",
          "BRCA2" = "Red",
          "CHEK2" = "Blue",
          "MSH6" = "Green",
          "NBN" = "Brown",
          "No_Mutation" = "lemonchiffon1",
          "PALB2" = "Purple",
          "PMS2" = "Light Grey",
          "POLD1" = "Light Green",
          "PRSS1" = "Violet",
          "RAD61D" = "Turquoise",
          "SLX4" = "Orange",
          "XRCC2" = "Yellow", 
          "Others" = "Grey"
        )
      )
  }, bg="transparent", execOnResize = TRUE)
  
  #------ Proportions --------#
  
  #Render UI 
  output$moreControls <- renderUI({
    if(input$Improvements2 && (length(input$Right_Proportions) < 11 && !(length(input$Right_Proportions) == 10 && !is.element("Not Considered", input$Right_Proportions)))) {
      if (!is.null(boolean()) && boolean() == TRUE) {
        materialSwitch(
          inputId = "OtherGroup",
          label = "Show Not Selected",
          status = "primary",
          right = TRUE,
          value = TRUE
        )
      } else {
        materialSwitch(
          inputId = "OtherGroup",
          label = "Show Not Selected",
          status = "primary",
          right = TRUE,
          value = FALSE
        )
      }
    }
  })
  
  bool<- FALSE
  
  boolean <- reactive({
    bool <- input$OtherGroup
  })
  
  Starting_Data <- read_excel("GenusData.xlsx")
  Starting_Data <- melt(Starting_Data, id.var = "Genus")
  
  #Reactive Equations for Proportions
  Variables_Selected <- reactive({
    req(input$Right_Proportions)
    dplyr::filter(Starting_Data, Genus %in% input$Right_Proportions)
  })
  
  Totals <- reactive({
    Total <- 0
    bool <- is.element("Not Considered", input$Right_Proportions)
    for (value in Variables_Selected()$value) {
      print(value)
      Total = Total + value
    }
    if (bool) {
      Total = Total
    } else {
      Total = Total + 0.5088
    }
    Total = Total
  })
  
  Totals2 <- reactive({
    Total <- 0
    bool <- is.element("Not Considered", input$Right_Proportions)
    for (value in Variables_Selected()$value) {
      print(value)
      Total = Total + value
    }
    Total = Total
  })
  
  observe({
    Totals()
    print("TOTALS")
    print(Totals()) 
  })
  
  Other_Column <- reactive ({
    Genus <- c("Not Selected")
    variable <-("Abundance")
    value <- c((1 - Totals()))
    otherDF <- data.frame(Genus, variable, value)
    random <- rbind(Variables_Selected(), otherDF)
  })
  
  output$Proportions  <- renderPlot ({
    if (input$Improvements2 == FALSE) {
      ggplot(data = Variables_Selected(), aes(
        x = Genus,
        y = (value / Totals2()),
        fill = Genus
      )) +
        geom_bar(stat = "identity", width = 0.7) +
        theme(
          legend.title = element_blank(),
          axis.text.x = element_text(angle = 90, hjust = 1)
        ) +
        scale_y_continuous(limits = c(0, input$proportionsSlider)) + ylab("Relative Abundance") +
        scale_fill_manual(
          values = c(
            "Bacteroides" = "coral4",
            "Mucinivorans" = "chartreuse3",
            "Prevotella" = "cadetblue3",
            "Streptococcus" = "brown3",
            "Treponema" = "blueviolet",
            "Staphylococcus" = "dodgerblue4",
            "Clostridium" = "deeppink3",
            "Helicobacter" = "darkorange3",
            "Turicibacter" = "khaki3",
            "Bacillus" = "goldenrod3",
            "Not Considered" = "yellow3",
            "Not Selected" = "105"
          )
        )
    } else if (!is.null(input$OtherGroup) && input$OtherGroup == TRUE) {
      ggplot(data = Other_Column(), aes(
        x = Genus,
        y = (value),
        fill = Genus
      )) +
        geom_bar(stat = "identity", width = 0.7) +
        theme(
          legend.title = element_blank(),
          axis.text.x = element_text(angle = 90, hjust = 1)
        ) +
        scale_y_continuous(limits = c(0, input$proportionsSlider)) + ylab("Relative Abundance") +
        scale_fill_manual(
          values = c(
            "Bacteroides" = "coral4",
            "Mucinivorans" = "chartreuse3",
            "Prevotella" = "cadetblue3",
            "Streptococcus" = "brown3",
            "Treponema" = "blueviolet",
            "Staphylococcus" = "dodgerblue4",
            "Clostridium" = "deeppink3",
            "Helicobacter" = "darkorange3",
            "Turicibacter" = "khaki3",
            "Bacillus" = "goldenrod3",
            "Not Considered" = "yellow3",
            "Not Selected" = "105"
          )
        )
    }
    else {
      ggplot(data = Variables_Selected(), aes(x = Genus, y = value, fill = Genus)) +
        geom_bar(stat = "identity", width = 0.7) +
        theme(
          legend.title = element_blank(),
          axis.text.x = element_text(angle = 90, hjust = 1)
        ) + scale_y_continuous(limits = c(0, input$proportionsSlider)) + ylab("Relative Frequency") +
        scale_fill_manual(
          values = c(
            "Bacteroides" = "coral4",
            "Mucinivorans" = "chartreuse3",
            "Prevotella" = "cadetblue3",
            "Streptococcus" = "brown3",
            "Treponema" = "blueviolet",
            "Staphylococcus" = "dodgerblue4",
            "Clostridium" = "deeppink3",
            "Helicobacter" = "darkorange3",
            "Turicibacter" = "khaki3",
            "Bacillus" = "goldenrod3",
            "Not Considered" = "yellow3",
            "Not Selected" = "105"
          )
        )
      
    }
  }, bg="transparent", execOnResize = TRUE)
  
  #---------- Airlines Box and Whiskers --------------# 
  airline_delay_causes <- read_excel("airline_delay_causes.xls")
  Janurary <-
    gather(airline_delay_causes,
           Type,
           delay,
           c(carrier_delay:late_aircraft_delay))
  Janurary2 <-
    gather(Janurary, Cause, Time, c(carrier_ct:late_aircraft_ct))
  
  UpdatesAirlines2 <- eventReactive(
    input$submit2,
    {Airlines_Selected2()
    }, ignoreNULL = FALSE
  )
  
  #Reactive Equations for Airport
  Airlines_Selected <- reactive({
    req(input$Pick_Airlines)
    dplyr::filter(Janurary2, carrier_name %in% input$Pick_Airlines)
  })
  
  type <- reactive({
    req(input$Pick_Type)
    dplyr::filter(Airlines_Selected(), Type %in% input$Pick_Type)
  })
  
  #YAxis <- reactive({
    #req(input$Y_Axis_Maximum2)
    #dplyr::filter(type(), delay <= input$Y_Axis_Maximum2)
  #})
  
  Airlines_Selected2 <- reactive({
    req(input$Pick_Airlines2)
    dplyr::filter(Janurary2, carrier_name %in% input$Pick_Airlines2) })
  
  UpdatesAirlines <- eventReactive(
    input$submit,
    {
      Airlines_Selected(); type(); #YAxis();
    }, ignoreNULL = FALSE
  )
  
  UpdatesAirlines2 <- eventReactive(
    input$submit2,
    {Airlines_Selected2()
    }, ignoreNULL = FALSE
  )
  
  output$Airline2 <- renderPlot({
    if (!input$Dots) {
      ggplot(data = UpdatesAirlines(), aes(x = carrier_name, y = delay, col = Type)) +
        geom_boxplot() +
        theme(axis.text.x = element_text(angle = 90, hjust = 1),
              axis.title.x = element_blank()) +
        coord_cartesian(ylim = c(0, input$Y_Axis_Maximum2)) +
        ylab("Number of Delays") +
        scale_fill_discrete(
          name = "Type",
          breaks = c(
            "carrier_delay",
            "late_aircraft_delay",
            "nas_delay",
            "security_delay",
            "weather_delay"
          ),
          labels = c("Carrier", "Late Aircraft", "NAS", "Security",
                     "Weather")
        )
    } else if (input$Dots) {
      ggplot(data = UpdatesAirlines(), aes(x = carrier_name, y = delay, col = Type)) +
        geom_boxplot(outlier.shape = NA) +
        theme(axis.text.x = element_text(angle = 90, hjust = 1),
              axis.title.x = element_blank()) +
        coord_cartesian(ylim = c(0, input$Y_Axis_Maximum2)) +
        ylab("Number of Delays") +
        scale_fill_discrete(
          name = "Type",
          breaks = c(
            "carrier_delay",
            "late_aircraft_delay",
            "nas_delay",
            "security_delay",
            "weather_delay"
          ),
          labels = c("Carrier", "Late Aircraft", "NAS", "Security",
                     "Weather")
        )
    }
  }, bg="transparent", execOnResize = TRUE)
  
  #----- Heatmap and Coloring ----#
  
  set.seed(9001)
  b <- rnorm(100, 40, 2)
  set.seed(9001)
  c <- rnorm(100, 20, 2)
  
  set.seed(9001)
  b2 <- rnorm(100, 40, 10)
  heatMatrix2 <- matrix (0, nrow = 12, ncol = 12)
  
  for (i in 1:nrow(heatMatrix2)) {
    for (j in 1:ncol(heatMatrix2)) {
      heatMatrix2[i, j] = sample(b2, 1)
    }}
  
  newColour <- melt(heatMatrix2)
  
  list_of_values <- c(0, 1)
  heatMatrix <- matrix (0, nrow = 12, ncol = 12)
  
  for (i in 1:nrow(heatMatrix)) {
    for (j in 1:ncol(heatMatrix)) {
      heatMatrix[i, j] = sample(list_of_values, 1)
    }
  }
  
  for (i in 1:nrow(heatMatrix)) {
    for (j in 1:ncol(heatMatrix)) {
      if (heatMatrix[i, j] == 0) {
        heatMatrix[i, j] = sample(b, 1)
      } else {
        heatMatrix[i, j] = sample(c, 1)
      }
    }
  }
  
  samples <- reactive ({
    set.seed(9001)
    b <- rnorm(100, 40 - 2 * input$Colour, 1)
    sample <- rbind(b, c)
    sample <- melt(sample)
  })
  
  matrix <- reactive ({
    set.seed(9001)
    b <- rnorm(100, 40 - 2 * input$Colour, 1)
    for (i in 1:nrow(heatMatrix)) {
      for (j in 1:ncol(heatMatrix)) {
        heatMatrix[i, j] = sample(list_of_values, 1)
      }
    }
    #FIRST
    for (i in 1:nrow(heatMatrix)) {
      for (j in 1:ncol(heatMatrix)) {
        if (heatMatrix[i, j] == 0) {
          heatMatrix[i, j] = sample(b, 1)
        } else {
          heatMatrix[i, j] = sample(c, 1)
        }
      }
    }
    HeatDF <-  melt(heatMatrix)
  })
  
  I <- reactive({
    ggplot(data = matrix(), aes(x = Var1, y = Var2)) +
      geom_tile(aes(fill = value)) +
      theme(
        plot.title = element_text(hjust = 0.5),
        axis.text.x = element_blank(),
        axis.text.y = element_blank(),
        axis.ticks  = element_blank(),
        axis.title.y = element_blank(),
        axis.title.x = element_blank(),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.background = element_blank()
      ) +
      scale_fill_gradient2(
        low = "Red",
        mid = "black",
        high = "Green",
        midpoint = ((40 - (2 * input$Colour) + 20) / 2)
      )
  })
  
  K <- reactive({
    ggplot(data = newColour, aes(x = Var1, y = Var2)) +
      geom_tile(aes(fill = value)) + 
      theme(
        plot.title = element_text(hjust = 0.5),
        axis.text.x = element_blank(),
        axis.text.y = element_blank(),
        axis.ticks  = element_blank(),
        axis.title.y = element_blank(),
        axis.title.x = element_blank(),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.background = element_blank()
      )    +   scale_fill_gradient2(
        low = "white",
        mid = "white",
        high = "darkgreen",
        midpoint = 10
      )
  })
  
  output$HeatMap <- renderPlot({
    if (!input$CorrectColour || input$Colour < 10) {
      I()
    } else {
      K()
    }
  }, bg="transparent", execOnResize = TRUE)
  
  output$Histogram <- renderPlot({
    if (!input$CorrectColour || input$Colour < 10) {
      legends = get_legend(I() +
                             theme(legend.direction = "horizontal",legend.justification="center", legend.title = element_blank(), 
                                   legend.text = element_blank(), legend.key.width =(unit((4 - 0.25*input$Colour), "cm"))))
    } else {
      legends = get_legend(K() +
                             theme(legend.direction = "horizontal",legend.justification="center", legend.title = element_blank(), 
                                   legend.text = element_blank(), legend.key.width =(unit((4 - 0.25*input$Colour), "cm"))))
    }
    
    H <-  ggplot(data = samples(), aes(x = value)) + geom_density(fill = "gray90") +
      geom_vline(aes(xintercept = (40 - (2 * input$Colour) + 20) / 2)) +
      theme(
        plot.title = element_text(face = "bold", hjust = 0.5),
        axis.text  = element_blank(),
        axis.ticks  = element_blank(),
        axis.title.y = element_blank(),
        axis.title.x = element_blank(),
        # panel.grid.major = element_blank(),
        # panel.grid.minor = element_blank(),
        panel.background = element_blank()
      ) + ggtitle("Distribution of the Data") + scale_x_continuous(limits=c(10, (50 - 2*input$Colour)))
    
    ##HH <-  ggplot(data = samples(), aes(x = value)) + geom_density(fill = "gray90") +
    ##  geom_vline(aes(xintercept = (40 - (2 * input$Colour) + 20) / 2)) +
    ##  theme(
    ##    plot.title = element_text(face = "bold", hjust = 0.5),
    ##    axis.text  = element_blank(),
    ##    axis.ticks  = element_blank(),
    ##    axis.title.y = element_blank(),
    ##    axis.title.x = element_blank(),
    ##    panel.background = element_blank()
    ##  ) + ggtitle("Distribution of the Data") + scale_x_continuous(limits=c(10, (50 - 2*input$Colour)))
    
    plot_grid(H,legends, ncol = 1, rel_heights = c(0.9, 0.1), rel_widths = c(1,1))
  }, bg="transparent", execOnResize = TRUE)
  
  #----- 3D Visualisations ----#
  
  densityM <- reactive({
    print(returned_Matrix(as.numeric(input$skew+1)))
  })
  
  output$D_graphs <- renderPlot ({
    lattice.options(panel.error="stop")

        cloud(as.numeric(as.character(Observation))~as.factor(Type_1)+as.factor(Type_2), densityM(), panel.3d.cloud=panel.3dbars,
          xbase=0.3,  
          ybase=0.3, 
          zlab = NULL, 
          col.facet=c("blue", "yellow", "purple", "orange", "green"),
          group = Type_1, 
          par.settings = list(layout.heights =
                                                list(top.padding = -40,
                                                     main.key.padding = -40,
                                                     key.axis.padding = -15,
                                                     axis.xlab.padding = -15,
                                                     xlab.key.padding = -15,
                                                     key.sub.padding = -15,
                                                     bottom.padding = -40),
                                               axis.line = list(col = 0),
                                               clip =list(panel="off"),
                                              layout.widths =
                                                list(left.padding = 0,
                                                     key.ylab.padding = 0,
                                                     ylab.axis.padding = -5,
                                                     axis.key.padding = -5,
                                                     right.padding = 0)
          ),
          scales=list(arrows=FALSE, col=1), xlab = NULL, ylab = NULL, main = NULL,
          par.box = list(col = NA), lcol=NULL
          )
  }, bg="transparent",execOnResize = TRUE, height = 500, width = 500)
  
  #---ScatterPlot Data and Visualisation ---#
  
  HDI <- read.csv(file="Human Development Index (HDI).csv", header = TRUE, sep=",", check.names = FALSE)
  HDI <- melt(HDI, id=c("HDI_Rank", "Country"))
  HDI <- na.omit(HDI)
  HDI$variable <- as.numeric(as.character(HDI$variable))
  HDI$HDI_RANK <- as.numeric(as.character(HDI$HDI_Rank))
  
  HDI_UP <- reactive ({ 
    years <- input$yearsRed
    HDI2 <- filter(HDI, HDI$variable >= years)
    HDI2$variable <- as.character(HDI2$variable)
    HDI2
  })
  
  HDI_UP2 <- reactive ({
    countries <- input$HDI_RANK
    HDI3 <- filter(HDI_UP(), HDI_UP()$HDI_Rank <= countries)
    HDI3
  })
  
  alpha <- reactive ({ 
    alpha <- input$alpha
    alpha
  })
  
  output$scatterJitter <- renderPlot ({
    if(input$removeSize) {
      if (input$jitter && ! input$LineGraph) {
        ggplot(HDI_UP2(), aes(x = variable, y = value, color = Country)) + geom_jitter(alpha = alpha()) + xlab("Year") + 
          ylab("HDI Value") + guides(size=guide_legend(nrow=1), color = guide_legend(nrow = 10)) + scale_y_continuous(limits=c(0.6, 1)) + theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
      } else if (!input$LineGraph) {
        ggplot(HDI_UP2(), aes(x = variable, y = value, color = Country)) + geom_point(alpha = alpha()) + xlab("Year") + 
          ylab("HDI Value") + guides(size=guide_legend(nrow=1), color = guide_legend(nrow = 10)) + scale_y_continuous(limits=c(0.6, 1)) + theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
      }  else {
        ggplot(HDI_UP2(), aes(x = variable, y = value, group = Country, color = Country)) + geom_line(alpha = alpha()) + geom_point(alpha = alpha()) + 
          xlab("Year") + ylab("HDI Value") + guides(size=guide_legend(nrow=1), color = guide_legend(nrow = 10)) + scale_y_continuous(limits=c(0.6, 1)) + theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
      }    
    } else {
      if (input$jitter && ! input$LineGraph) {
        ggplot(HDI_UP2(), aes(x = variable, y = value, color = Country, size = HDI_Rank)) + geom_jitter(alpha = alpha()) + 
          xlab("Year")+ ylab("HDI Value") + guides(size=guide_legend(nrow=1), color = guide_legend(nrow = 10)) + scale_y_continuous(limits=c(0.6, 1)) + theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
      } else if (!input$LineGraph) {
        ggplot(HDI_UP2(), aes(x = variable, y = value, color = Country, size = HDI_Rank)) + geom_point(alpha = alpha()) + 
          xlab("Year")+ ylab("HDI Value") + guides(size=guide_legend(nrow=1), color = guide_legend(nrow = 10)) + scale_y_continuous(limits=c(0.6, 1)) + theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
      } else {
        ggplot(HDI_UP2(), aes(x = variable, y = value, group = Country, color = Country)) + geom_line(alpha = alpha()) + geom_point(alpha = alpha()) + 
          xlab("Year")+ ylab("HDI Value") + guides(size=guide_legend(nrow=1), color = guide_legend(nrow = 10)) + scale_y_continuous(limits=c(0.6, 1)) + theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
      }
    }
  }, bg="transparent",execOnResize = TRUE)
  
  
  ## OLD CODE FOR AIRLINES SCATTER PLOT
  output$Airline3 <- renderPlot({
    ggplot(data = UpdatesAirlines2(), aes(x = arr_delay, y = arr_del15, col = carrier_name)) +
      geom_point(alpha = 0.05) + coord_cartesian(ylim = c(0, 100), xlim = c(0,input$X_Axis_Maximum)) +
      xlab("Total Number of Delays") + ylab("Total Delay Time")
    
  }, bg="transparent", execOnResize = TRUE)
  
  
})
